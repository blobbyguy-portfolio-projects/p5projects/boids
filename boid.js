class Boid {
    position;
    acceleration;
    velocity;
    constructor(position, visionRadius, debug = false){
        this.position = position;
        this.visionRadius = visionRadius;
        this.acceleration = createVector(0,0);
        this.velocity = createVector(0,0);
        this.debug = debug;
    }

    Draw(){
        this.Update();
        stroke(255);
        fill(this.debug ? color(255, 0,0 ) : 255);
        circle(this.position.x, this.position.y, 10);

        if (this.debug){
            stroke(color(color(255, 0, 0)))
            noFill()
            circle(this.position.x, this.position.y, this.visionRadius * 2);
        }
    }

    ApplyForce(force){
        this.acceleration.add(force);
        this.ClampAcceleration(this.maxAcceleration);
    }

    Update(){
        this.position = this.position.add(this.velocity);
        this.velocity = this.velocity.add(this.acceleration);
        this.ClampVelocity(this.maxVelocity);
        this.acceleration.mult(0);
    }

    CorrectPosition(width, height, allowWrap){
        if (allowWrap){
            this.position.x = this.position.x < 0 ? width : this.position.x % width;
            this.position.y = this.position.y < 0 ? height : this.position.y % height;
        }else{
            const clamp = (num, min, max) => Math.min(Math.max(num, min), max);
            this.position.x = clamp(this.position.x, 0, width);
            this.position.y = clamp(this.position.y, 0, height);
        }

    }

    ClampVelocity(velocity){
        this.velocity.limit(velocity);
    }
    ClampAcceleration(maxAcceleration){
        this.acceleration.limit(maxAcceleration);
    }

    CalculateVectorTo(position){
        return createVector(
            position.x - this.position.x,
            position.y - this.position.y
        );
    }

    CalculateDistanceFrom(neigbbour){
        return neigbbour.position.dist(this.position);
    }

    ApplyBoids(neighbours, maxAcceleration, maxVelocity, seperationWeight, alignmentWeight, cohesionWeight){
        this.maxVelocity = maxVelocity;
        this.maxAcceleration = maxAcceleration;
        this.ApplyCohesion(neighbours, cohesionWeight);
        this.ApplySeperation(neighbours, seperationWeight);
        this.ApplyAlignment(neighbours, alignmentWeight);
    }

    ApplySeperation(neighbours, seperationWeight){
        var avg = createVector(0,0);
        for (var i = 0; i < neighbours.length; i++){
            //The closer you are, the more force gets applied
            let dist = this.CalculateDistanceFrom(neighbours[i]);

            let diff = p5.Vector.sub(this.position, neighbours[i].position);
            if (dist == 0){
                continue;
            }
            diff.mult(1/dist);
            avg.add(diff);
        }

        // let steering = neighbours.length == 0 ? 
        // 0
        // : avg.div(neighbours.length);
        this.ApplyForce(avg.mult(seperationWeight));
    }

    ApplyAvoidWalls(){
        //Avoid top wall

        //Avoid bottom wall

        //Avoid left wall

        //Avoid right wall
    }

    //Steer towards average heading
    ApplyAlignment(neighbours, alignmentWeight){
        var averageHeading = this.CalculateAverageVelocity(neighbours);
        this.ApplyForce(averageHeading.mult(alignmentWeight));
    }

    //Steer towards average position
    ApplyCohesion(neighbours, cohesionWeight){
        let averagePos = this.CalculateAveragePosition(neighbours);
        let vectorToAverage = this.CalculateVectorTo(averagePos);
        if (this.debug){
            console.log("TEST");
            console.log({vectorToAverage, cohesionWeight});
        }
        this.ApplyForce(vectorToAverage.mult(cohesionWeight));
    }

    //TODO - Could probably extract these out to helper
    CalculateAverageVelocity(neighbours){
        var avg = createVector(0,0);
        
        for (var i = 0; i < neighbours.length; i++){
            avg.add(neighbours[i].velocity.x, neighbours[i].velocity.y);
        }
        return neighbours.length == 0 ? 
        0
        : avg.div(neighbours.length)
    }

    CalculateAveragePosition(neighbours){
        var avg = createVector(0,0);
        for (var i = 0; i < neighbours.length; i++){
            avg.add(neighbours[i].position.x, neighbours[i].position.y);
        }
        return neighbours.length == 0 ? 
        0
        : avg.div(neighbours.length)
    }
}