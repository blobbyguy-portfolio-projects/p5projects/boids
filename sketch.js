let width = screen.width;
let height = screen.height - 120;

let initialExplodeForce = 10;
let maxVelocity = 2.5;
let maxAcceleration = 10;

let visionEnabled = true;
let boidVision = 300;

let numberOfBoids = 100;
var boids;



var seperationSlider;
var alignmentSlider;
var cohesionSlider;

var debugCheckbox;
var wallWrapCheckbox;

function setup() {
  createCanvas(width, height);

  seperationDiv = createDiv('seperation');
  cohesionDiv = createDiv('cohesion');
  alignmentDiv = createDiv('alignment');

  debugCheckbox = createCheckbox("Show Debug", true);
  wallWrapCheckbox = createCheckbox("Allow Wrapping", false);


  seperationSlider = createSlider(0, 2, 1, 0.1);
  alignmentSlider = createSlider(0, 2, 1, 0.1);
  cohesionSlider = createSlider(0, 2, 1, 0.1);

  seperationSlider.parent(seperationDiv);
  cohesionSlider.parent(cohesionDiv);
  alignmentSlider.parent(alignmentDiv);

  boids = new Array(numberOfBoids);
  for (var x = 0; x < numberOfBoids; x++){
    var pos = createVector(width/2, height/2);

    boids[x] = new Boid(createVector(random(0, width), random(0, height)), boidVision, false);

    boids[x].ApplyForce(
      createVector(
        random(-initialExplodeForce,initialExplodeForce), 
        random(-initialExplodeForce,initialExplodeForce)));
  }
  boids[0].debug = true;
}

function draw() {
  background(0);

  boids[0].debug = debugCheckbox.checked();

  var seperationWeight = seperationSlider.value();
  var alignmentWeight =  alignmentSlider.value();
  var cohesionWeight =  cohesionSlider.value();

  for (var x = 0; x < numberOfBoids; x++){
    boids[x].CorrectPosition(width, height, wallWrapCheckbox.checked());
    boids[x].ApplyBoids(getNeighbours(boids[x]), maxAcceleration, maxVelocity, seperationWeight, alignmentWeight, cohesionWeight);
  }

  for (var x = 0; x < numberOfBoids; x++){
    boids[x].Update();

    boids[x].Draw();
  }
}

function getNeighbours(boid){
  var neighbours = new Array();
  for (var x = 0; x < numberOfBoids; x++){
    if (this.boids[x] == this){
      continue;
    }

    if (this.boids[x].CalculateDistanceFrom(boid) < boidVision){
      neighbours.push(this.boids[x]);
      if (boids[x].debug){
        stroke(color(255,0,0));
        line(boids[x].position.x, boids[x].position.y, boid.position.x, boid.position.y);
      }
    }

  }
  return neighbours;
}
